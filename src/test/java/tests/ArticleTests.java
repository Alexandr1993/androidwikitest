package tests;

import listener.ListenerTest;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.MainPage;
public class ArticleTests extends BaseTest {
    @Test(description = "Проверка перехода на страницу статьи и названия статьи")
    public void checkTransAndArticle() {
        Assert.assertEquals(
                new MainPage()
                        .buttonSkip()
                        .clickSearch()
                        .setTextFind("Java")
                        .clickArticle(3)
                        .getTextArticle(), "Java (programming language)");
    }

    @Test(description = "Проверка свайпа вверх")
    public void checkSwipe() {
        new MainPage()
                .buttonSkip()
                .clickSearch()
                .setTextFind("Java")
                .clickArticle(3)
                .swipeUp();
    }

    @Test(description = "Проверка свайпа до элемента")
    public void checkSwipeToElement() {
        new MainPage()
                .buttonSkip().clickSearch()
                .setTextFind("Java")
                .clickArticle(3)
                .swipeToElement("View article in browser");
    }

    @Test(description = "Проверка элемента title")
    public void checkTitleArticle() {
        new MainPage()
                .buttonSkip()
                .clickSearch()
                .setTextFind("Java")
                .clickArticle(3)
                .assertTitlePresent();
    }


}
