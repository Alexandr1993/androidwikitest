package tests;

import driver.Platform;
import listener.ListenerTest;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.MainPage;
public class MyListsTests extends BaseTest {

    @Test(description = "Проверка добавления статьи и удаления статьи")
    public void checkAddArticleAndRemoveArticle() {
        if (Platform.getInstance().isMw()) {
            return;
        }
        new MainPage()
                .buttonSkip()
                .clickSearch()
                .setTextFind("Java")
                .clickArticle(3)
                .clickButtonSave()
                .clickButtonSave()
                .clickAddToList()
                .clickCreateNew()
                .setTextName("List Article Java")
                .clickButtonOk()
                .clickButtonBack()
                .clickButtonBack()
                .clickButtonSaveList()
                .clickList("List Article Java")
                .checkNameArticleToList("Java (programming language)")
                .swipeElementLeft(1)
                .checkRemoveElement();

    }


    @Test(description = "Проверка добавления двух статей и удаления одной")
    public void checkAddTwoArticleAndRemoveOneArticle() {
        if (Platform.getInstance().isMw()) {
            return;
        }
        Assert.assertEquals(new MainPage()
                .buttonSkip().clickSearch()
                .setTextFind("Java")
                .clickArticle(1)
                .clickButtonSave()
                .clickButtonSave()
                .clickAddToList()
                .clickCreateNew()
                .setTextName("Java")
                .clickButtonOk()
                .clickButtonBack()
                .clickArticle(3)
                .clickButtonSave()
                .clickButtonSave()
                .clickAddToList()
                .clickAddOnList("Java")
                .clickButtonBack()
                .clickButtonBack()
                .clickButtonSaveList()
                .clickList("Java")
                .swipeElementLeft(1)
                .checkNameArticleToList("Java (programming language)")
                .clickNameArticle(1)
                .getTextArticle(), "Java (programming language)");
    }

}
