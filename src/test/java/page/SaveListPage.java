package page;

import factories.ArticlePageFactory;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.testng.Assert;

public abstract class SaveListPage extends Page {

    protected static String nameArticleToList;
    protected static String nameList;

    /* Метод для нажатия на выбранный список статей */
    @Step("Нажатие на список статей")
    public SaveListPage clickList(String nameListArticle) {
        waitForElementAndClick(replaceSubstringInLocator(nameListArticle, nameList), "List not found");
        return this;
    }

    /* Метод для проверки названия статьи в списке */
    @Step("Проверка имени статьи {name}")
    public SaveListPage checkNameArticleToList(String name) {
        Assert.assertEquals(waitForElementAndGetText(nameArticleToList,
                "Name article to list not found"), name);
        return this;
    }

    /* Свайп элемента влево */
    @Step("Свайп статьи номер {numberArticle} списке")
    public SaveListPage swipeElementLeft(int numberArticle) {
        waitForElement(nameArticleToList, "Name Article not found");
        appiumSwipeElement(remoteDriver.findElements(getByLocator(nameArticleToList)).get(numberArticle - 1),
                5000, 1, "left");
        return this;
    }

    /* Проверка, что статья удалена */
    @Step("Проверка была ли удалена статья из списка")
    public SaveListPage checkRemoveElement() {
        Assert.assertTrue(waitForElementNotPresent(nameArticleToList,
                "Name Article found", 4));
        return this;
    }

    /* Клик по статье */
    @Step("Нажатие на статью под номером {numberArticle} в списке")
    public ArticlePage clickNameArticle(int numberArticle) {
        waitForElement(nameArticleToList, "Name Article not found");
        remoteDriver.findElements(getByLocator(nameArticleToList)).get(numberArticle - 1).click();
        return new ArticlePageFactory().get();
    }



}
