package web;

import page.SearchPage;

public class WebSearchPage extends SearchPage {

    static {
        xpathFindSearch = "xpath://input[@class='search mw-ui-background-icon-search']";
        xpathResultSearchJava = "xpath://div[contains(text(), '{SUBSTRING}')]";
        closeButton = "xpath://button[contains(text(), 'Clear')]";
        resultSearch = "xpath://h3";
        textNoResults = "xpath://p[text() ='No page with this title.']";
        buttonBack = "className:android.widget.ImageButton";
    }

}
