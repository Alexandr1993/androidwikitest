package tests;

import driver.MyDriver;
import io.appium.java_client.android.AndroidDriver;
import listener.ListenerTest;
import org.openqa.selenium.ScreenOrientation;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

@Listeners(ListenerTest.class)
public class BaseTest {

    MyDriver driver = new MyDriver();

    @BeforeMethod
    public void setUp() throws Exception {
        driver.setupDriver();
    }

    @AfterMethod
    public void tearDown() {
        if (MyDriver.getDriver() instanceof AndroidDriver) {
            ((AndroidDriver) MyDriver.getDriver()).rotate(ScreenOrientation.PORTRAIT);
        }
        else {
            System.out.println("Метод для данной платформы не используется");
        }
        driver.closeDriver();
    }

}
