package page;

import driver.MyDriver;
import com.google.common.collect.ImmutableMap;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Attachment;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;


public class Page {
    protected RemoteWebDriver remoteDriver = MyDriver.getDriver();
    private Dimension size = remoteDriver.manage().window().getSize(); // размер экрана

    protected  WebElement waitForElement(String locator, String errorMessage, long timeoutInSeconds) {
        WebDriverWait wait = new WebDriverWait(remoteDriver, Duration.ofMillis(timeoutInSeconds*1000));
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.presenceOfElementLocated(getByLocator(locator)));
    }

    protected  WebElement waitForElement(String locator, String errorMessage) {
        return waitForElement(locator, errorMessage, 5);
    }

    protected WebElement waitForElementAndClick(String locator, String errorMessage) {
        WebElement element = waitForElement(locator, errorMessage);
        element.click();
        return element;
    }

    protected WebElement waitForElementAndSend(String locator, String errorMessage, String text, long timeoutInSeconds) {
        WebElement element = waitForElement(locator, errorMessage, timeoutInSeconds);
        element.sendKeys(text);
        return element;
    }

    protected  boolean waitForElementNotPresent(String locator, String errorMessage, long timeoutInSeconds) {
        WebDriverWait wait = new WebDriverWait(remoteDriver, Duration.ofMillis(timeoutInSeconds*1000));
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.invisibilityOfElementLocated(getByLocator(locator)));
    }

    protected String waitForElementAndGetText(String locator, String errorMessage) {
        WebElement element = waitForElement(locator, errorMessage, 15);
//        return element.getAttribute("text");
        return element.getText();
    }

    protected void swipe(PointOption start, PointOption end, int timeSwipe) {
        new TouchAction((PerformsTouchActions) remoteDriver)
                .press(start)
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(timeSwipe)))
                .moveTo(end)
                .release()
                .perform();
    }
    @Deprecated
    protected void deprecatedSwipeUp(int timeSwipe) {
        PointOption pointOptionStart, pointOptionEnd;
        pointOptionStart = PointOption.point(size.width/2, (int) (size.height*0.8));
        pointOptionEnd = PointOption.point(size.width/2, (int) (size.height*0.2));
        swipe(pointOptionStart, pointOptionEnd, timeSwipe);
    }

    protected void appiumSwipe(double edgeBorderLeftPercent,
                            double edgeBorderTopPercent,
                            double edgeWidthPercent,
                            double edgeHeigthPercent,
                            int speed,
                            double percent,
                            String direction) {

        int edgeBorderLeft = (int) (size.width * edgeBorderLeftPercent);
        int edgeBorderTop = (int) (size.height * edgeBorderTopPercent);
        int edgeWidth = (int) (size.width - edgeWidthPercent * edgeBorderLeft);
        int edgeHeigth = (int) (size.height - edgeHeigthPercent * edgeBorderTop);
        ((JavascriptExecutor) remoteDriver).executeScript("mobile: swipeGesture", ImmutableMap.of(
                "left", edgeBorderLeft,
                "top", edgeBorderTop,
                "width", edgeWidth,
                "height", edgeHeigth,
                "percent", percent,
                "speed", speed,
                "direction", direction)
        );

    }
    protected void appiumSwipe(int speed, double percent, String direction) {
        if (remoteDriver instanceof AndroidDriver) {
            appiumSwipe(0.2, 0.2, 2, 2,
                    speed, percent, direction);
        }
        else {
            System.out.println("Для данной платформы метод не используется");
        }
    }

    protected void appiumSwipeToElement(String locator, int maxSwipe, String errorMessage, int speed, String direction) {
        int realySwipe = 0;
        if (remoteDriver instanceof AndroidDriver) {
            while (remoteDriver.findElements(getByLocator(locator)).size() == 0) {
                if (realySwipe > maxSwipe) {
                    waitForElement(locator, "Cannot find element by swiping up. \n" + errorMessage, 10);
                    return;
                }
                appiumSwipe(speed, 1, direction);
                ++realySwipe;
            }
        }
        else {
            System.out.println("Для данной платформы метод не используется");
        }
    }

    protected void appiumSwipeElement(WebElement element, int speed, double percent, String direction) {
        ((JavascriptExecutor) remoteDriver).executeScript("mobile: swipeGesture", ImmutableMap.of(
                "percent", percent,
                "speed", speed,
                "direction", direction,
                "elementId", ((RemoteWebElement) element).getId())
        );
    }

    public Page rotate(String rotateType) {
        if (remoteDriver instanceof AndroidDriver) {
            if (rotateType == "LANDSCAPE") {
                ((AndroidDriver)remoteDriver).rotate(ScreenOrientation.LANDSCAPE);
            } else {
                ((AndroidDriver)remoteDriver).rotate(ScreenOrientation.PORTRAIT);
            }
        }
        else {
            System.out.println("Метод для данной платформы не используется");
        }
        return this;
    }

    public Page background(int timeSecond) {
        if (remoteDriver instanceof AndroidDriver) {
            ((AndroidDriver)remoteDriver).runAppInBackground(Duration.ofSeconds(timeSecond));
        }
        else {
            System.out.println("Метод для данной платформы не используется");
        }
        return this;
    }

    protected void assertElementPresent(String locator) {
        if (remoteDriver.findElements(getByLocator(locator)).size() == 0) {
            throw new AssertionError("Element " + locator.toString() + " not found");
        }
    }

    protected String replaceSubstringInLocator(String substring, String locator) {
        return locator.replace("{SUBSTRING}", substring);
    }

    /* Метод для получения коллекции элементов */
    protected List<WebElement> getCollectionResult(String locator, String errotMessage) {
        waitForElement(locator, errotMessage, 15);
        return remoteDriver.findElements(getByLocator(locator));
    }

    /* Метод для создания скриншота */
    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveAllureScreenshot() {
        return ((TakesScreenshot) remoteDriver).getScreenshotAs(OutputType.BYTES);
    }

    /* Метод для определения типа локатора */
    public By getByLocator(String locator) {
        String byType = locator.split(":", 2)[0];
        String byLocator = locator.split(":", 2)[1];
        if (byType.equals("id")) {
            return By.id(byLocator);
        }
        if (byType.equals("xpath")) {
            return By.xpath(byLocator);
        }
        if (byType.equals("className")) {
            return By.className(byLocator);
        }
        else throw new IllegalArgumentException("Cannot get typ of locator: " + locator);
    }


}
