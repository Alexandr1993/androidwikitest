package page;

import driver.Platform;
import factories.WikiPageFactory;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class MainPage extends Page {

    private static final String skip = "xpath://*[contains(@text, 'SKIP')]";
    /* Метод для нажатия на кнопку SKIP */
    @Step("Нажатие на кнопку SKIP")
    public WikiPage buttonSkip() {
        if (Platform.getInstance().isAndroid()) {
            waitForElementAndClick(skip, "Skip button not found");
            return new WikiPageFactory().get();
        }
        else
            return new WikiPageFactory().get();
    }

}
