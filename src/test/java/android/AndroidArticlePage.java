package android;

import page.ArticlePage;

public class AndroidArticlePage extends ArticlePage {

    static {
        title = "xpath://*[@resource-id='pcs-edit-section-title-description']/../android.view.View[1]";
        futureText = "xpath://*[contains(@text, \"{SUBSTRING}\")]";
        buttonSave = "id:org.wikipedia:id/page_save";
        buttonAddToList = "xpath://*[contains(@text, \"Add to another reading list\")]";
        buttonCreateNew = "xpath://*[contains(@text, \t\"Create new\")]";
        fieldNameList = "id:org.wikipedia:id/text_input";
        buttonOk = "id:android:id/button1";
        buttonBack = "className:android.widget.ImageButton";
        nameList = "xpath://*[@text = '{SUBSTRING}']";
    }

}
