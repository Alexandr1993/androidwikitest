package tests;

import factories.ArticlePageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.ArticlePage;
import page.MainPage;
import page.SearchPage;
public class ChangeAppConditionTests extends BaseTest {

    @Test(description = "Проверка background")
    public void checkBackGround() {
        SearchPage searchPage =
                new MainPage()
                        .buttonSkip()
                        .clickSearch()
                        .setTextFind("Java")
                        .checkResultJava("Object-oriented");
        searchPage.background(2);
        searchPage.checkResultJava("Object-oriented");
    }

    @Test(description = "Проверка названия статьи после изменение ориентации экрана")
    public void checkRotationArticleTitle() {
        ArticlePage articlePage = new ArticlePageFactory().get();
        String textArticleRotationLand =
                new MainPage()
                        .buttonSkip()
                        .clickSearch()
                        .setTextFind("Java")
                        .clickArticle(3)
                        .getTextArticle();
        articlePage.rotate("LANDSCAPE");
        String textArticleRotationPort = articlePage.getTextArticle();
        Assert.assertEquals(textArticleRotationLand, textArticleRotationPort, "Article title not received");
        articlePage.rotate("PORTRAIT");
        Assert.assertEquals(textArticleRotationLand, articlePage.getTextArticle(), "Article title not received");

    }

}
