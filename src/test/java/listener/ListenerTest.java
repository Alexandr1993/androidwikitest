package listener;

import io.qameta.allure.model.StepResult;
import org.testng.*;
import page.Page;

public class ListenerTest implements IInvokedMethodListener {
    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (!testResult.isSuccess()) {
            new Page().saveAllureScreenshot();
        }
    }
}
