package factories;

import android.AndroidWikiPage;
import driver.Platform;
import page.WikiPage;
import web.WebWikiPage;

public class WikiPageFactory {

    public WikiPage get() {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidWikiPage();
        }
        else {
            return new WebWikiPage();
        }

    }

}
