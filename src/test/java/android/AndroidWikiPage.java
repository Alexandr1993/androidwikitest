package android;

import page.WikiPage;

public class AndroidWikiPage extends WikiPage {
    static {
        findSearch = "xpath://*[contains(@text, 'Search Wikipedia')]";
        buttonSaveList = "id:org.wikipedia:id/nav_tab_reading_lists";
    }
}
