package web;

import page.WikiPage;

public class WebWikiPage extends WikiPage {

    static {
        findSearch = "id:searchIcon";
        buttonSaveList = "id:org.wikipedia:id/nav_tab_reading_lists";
    }

}
