package android;

import page.SearchPage;

public class AndroidSearchPage extends SearchPage {
    static {
        xpathFindSearch = "xpath://*[@resource-id=\"org.wikipedia:id/search_src_text\"]";
        xpathResultSearchJava = "xpath://*[contains(@text, \"{SUBSTRING}\")]";
        closeButton = "id:org.wikipedia:id/search_close_btn";
        resultSearch = "id:org.wikipedia:id/page_list_item_title";
        textNoResults = "xpath://*[@text='No results']";
        buttonBack = "className:android.widget.ImageButton";
    }

}
