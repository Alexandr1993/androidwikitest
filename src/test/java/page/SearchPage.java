package page;

import factories.ArticlePageFactory;
import factories.WikiPageFactory;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;

import java.util.List;


public abstract class SearchPage extends Page {
    protected static String xpathFindSearch;
    protected static String xpathResultSearchJava;
    protected static String closeButton;
    protected static String resultSearch;
    protected static String textNoResults;
    protected static String buttonBack;
    /*Метод для ввода запрос в строку поиска*/
    @Step("Ввод в строку поиска text")
    public SearchPage setTextFind(String text) {
        waitForElementAndSend(xpathFindSearch,
                "Search input string not found", text, 15);
        return this;
    }

    /*Метод для нажатия на кнопку закрытия поиска*/
    @Step("Нажатие на кнопку закрытия поиска")
    public SearchPage clickCloseButton() {
        waitForElementAndClick(closeButton, "Close button not found");
        waitForElementNotPresent(closeButton, "close button present", 4);
        return this;
    }

    /*Метод для проверки результата поиска*/
    @Step("Проверка результата поиска")
    public SearchPage checkResultJava(String substring) {
        waitForElement(replaceSubstringInLocator(substring, xpathResultSearchJava),
                "Cannot find search result Java",
                15);
        return this;
    }

    /*Метод для очистки строки поиска*/
    @Step("Очистить стркоу поиска")
    public SearchPage clearSearch() {
        waitForElement(xpathFindSearch, "Search input string not found").clear();
        return this;
    }

//    public ArticlePage clickArticle() {
//        waitForElementAndClick(By.xpath(xpathResultSearchJava), "Article not found");
//        return new ArticlePage();
//    }

    /*Метод для клика по нужной статье*/
    @Step("Нажатие на статью номер {number}")
    public ArticlePage clickArticle(int number) {
        waitForElement(resultSearch, "Article not found");
        remoteDriver.findElements(getByLocator(resultSearch)).get(number - 1).click();
        return new ArticlePageFactory().get();
    }

    /*Метод для получения коллекции элементов поиска*/
    @Step("Получение количество найденных статей")
    public List<WebElement> getCollectionResultSearch() {
        return getCollectionResult(resultSearch, "Result not found");
    }

    /*Метод для проверки введенного слова в строку поиска*/
    @Step("Проверка, что введенное слово для поиска присутсвует в результатах поиска")
    public boolean checkWordSearch(String text) {
        boolean result = false;
        for (int i = 0; i < getCollectionResultSearch().size(); i++)
        {
            if (getCollectionResultSearch().get(i).getText()
                    .toLowerCase().contains(text.toLowerCase())) result = true;
            else return false;
        }
        return result;
    }

    /*Метод для проверки, что результатов поиска нет*/
    @Step("Проверка, что поиск не дал результатов")
    public SearchPage assertElementNotPresent() {
        if (remoteDriver.findElements(getByLocator(resultSearch)).size() > 0) {
            throw new AssertionError("Search results are present");
        }
        return this;
    }

    /*Метод для проверки текста "Not Result"*/
    @Step("Проверка текста Not Result на странице поиска")
    public SearchPage checkTextNotResult() {
        waitForElement(textNoResults, "Text Not Results not found");
        return this;
    }

    /*Метод для нажатия на кнопку назад*/
    @Step("Нажатие на кнопку назад")
    public WikiPage clickButtonBack() {
        waitForElementAndClick(buttonBack, "Button back not found");
        return new WikiPageFactory().get();
    }

}
