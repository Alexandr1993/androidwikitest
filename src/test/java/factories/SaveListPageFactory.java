package factories;

import android.AndroidArticlePage;
import android.AndroidSaveListPage;
import driver.Platform;
import page.ArticlePage;
import page.SaveListPage;
import web.WebArticlePage;
import web.WebSaveListPage;

public class SaveListPageFactory {
    public SaveListPage get() {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidSaveListPage();
        } else {
            return new WebSaveListPage();
        }
    }
}
