package android;

import page.SaveListPage;

public class AndroidSaveListPage extends SaveListPage {

    static {
        nameArticleToList = "id:org.wikipedia:id/page_list_item_title";
        nameList = "xpath://*[contains(@text, '{SUBSTRING}')]";
    }

}
