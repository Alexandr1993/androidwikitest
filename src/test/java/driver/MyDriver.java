package driver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class MyDriver {

    private static RemoteWebDriver driver;

//    protected void openWikiWebPageForMobileWeb() {
//        if (MyDriver.getDriver() instanceof ChromeDriver) {
//            driver.get("https://en.m.wikipedia.org");
//        }
//        else {
//            System.out.println("Метод для данной платформы ничего не делает");
//        }
//    }
    public void setupDriver() throws Exception {
        driver = Platform.getInstance().getDriver();
        if (driver instanceof ChromeDriver) {
            driver.get("https://en.m.wikipedia.org");
        }
    }

    public void closeDriver() {
        driver.quit();
    }

    public static RemoteWebDriver getDriver() {
        return driver;
    }
}
