package page;

import factories.SearchPageFactory;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Step;
import org.openqa.selenium.By;


public abstract class ArticlePage extends Page {

//    private static final String title = "//android.view.View[contains(@text, 'Java (programming language)')]";
    protected static String title;
    protected static String futureText;
    protected static String buttonSave;
    protected static String buttonAddToList;
    protected static String buttonCreateNew;
    protected static String fieldNameList;
    protected static String buttonOk;
    protected static String buttonBack;
    protected static String nameList;

    /* Метод для получения названия статьи */
    @Step("Получение названия статьи")
    public String getTextArticle() {
        return waitForElementAndGetText(title, "Article not found");
    }

    /* Свайп вверх */
    @Step("Свайп вверх")
    public ArticlePage swipeUp() {
        waitForElement(title, "Article not found", 20);
        appiumSwipe(500, 1, "up");
        return this;
    }

    /* Свайп до элемента */
    @Step("Свайп до элемента {textElement}")
    public ArticlePage swipeToElement(String textElement) {
        waitForElement(title, "Article not found", 10);
        appiumSwipeToElement(replaceSubstringInLocator(textElement, futureText), 200,
                "Cannot find article", 20000, "up");
        return this;
    }

    /* Метод для клика по кнопке сохранить статью  */

    @Step("Нажатие на кнопку сохранить статью")
    public ArticlePage clickButtonSave() {
        waitForElementAndClick(buttonSave, "Button Save not found");
        return this;
    }

    /* Метод для клика по кнопке добавить список для статей*/
    @Step("Нажатие на кнопку добавить список для статей")
    public ArticlePage   clickAddToList() {
        waitForElementAndClick(buttonAddToList, "Button Add to list not found");
        return this;
    }

    /* Метод для нажатия на кнопку создать новый список для статей */
    @Step("Нажатие на кнопку создать новый список для статей")
    public ArticlePage clickCreateNew() {
        waitForElementAndClick(buttonCreateNew, "Create New not found");
        return this;
    }

    /* Метод для ввода имени нового списка статей */
    @Step("Ввести название списка статей {text}")
    public ArticlePage setTextName(String text) {
        waitForElementAndSend(fieldNameList, "Field name list not found", text, 15);
        return this;
    }

    /* Метод для нажатия на кнопку ОК */
    @Step("Нажатие на кнопку ОК")
    public ArticlePage clickButtonOk() {
        waitForElementAndClick(buttonOk, "Button OK not found");
        return this;
    }

    /* Метод для нажатия на кнопку назад */
    @Step("Нажатие на кнопку назад")
    public SearchPage clickButtonBack() {
        waitForElementAndClick(buttonBack, "Button back not found");
        return new SearchPageFactory().get();
    }

    /* Метод для нажатия на кнопку добавить элемент в существущий список*/
    @Step("Нажатие на кнопку для добавления статьи в список {name}")
    public ArticlePage clickAddOnList(String name) {
        waitForElementAndClick(replaceSubstringInLocator(name, nameList),
                "List" + name + "not found");
        return this;
    }

    /* Метод для проверки существования title */
    @Step("Проверка имеется ли на странице элемент title")
    public ArticlePage assertTitlePresent() {
        assertElementPresent(title);
        return this;
    }


}
