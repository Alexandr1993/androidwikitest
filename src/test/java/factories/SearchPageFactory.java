package factories;

import android.AndroidSearchPage;
import driver.Platform;
import org.openqa.selenium.remote.RemoteWebDriver;
import page.SearchPage;
import web.WebSearchPage;

public class SearchPageFactory {
    public SearchPage get() {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidSearchPage();
        }
        else {
            return new WebSearchPage();
        }

    }

}
