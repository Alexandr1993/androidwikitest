package factories;

import android.AndroidArticlePage;
import android.AndroidSearchPage;
import driver.Platform;
import page.ArticlePage;
import page.SearchPage;
import web.WebArticlePage;
import web.WebSearchPage;

public class ArticlePageFactory {
    public ArticlePage get() {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidArticlePage();
        } else {
            return new WebArticlePage();
        }
    }
}
