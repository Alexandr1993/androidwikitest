package web;

import page.SaveListPage;

public class WebSaveListPage extends SaveListPage {

    static {
        nameArticleToList = "id:org.wikipedia:id/page_list_item_title";
        nameList = "xpath://*[contains(@text, '{SUBSTRING}')]";
    }

}
