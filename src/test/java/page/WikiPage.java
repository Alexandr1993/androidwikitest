package page;

import driver.Platform;
import factories.SaveListPageFactory;
import factories.SearchPageFactory;
import io.qameta.allure.Step;

public abstract class WikiPage extends Page {
    protected static String findSearch;
    protected static String buttonSaveList;

    /*Нажатие на строку поиска*/
    @Step("Нажатие на строку поиска")
    public SearchPage clickSearch() {
        waitForElementAndClick(findSearch, "Search string not found");
        return new SearchPageFactory().get();
    }

    /*Метод для получения текста в строке поиска Search Wikipedia*/
    @Step("Получение текста строки поиска")
    public String getTextArticle() {
        return waitForElementAndGetText(findSearch, "Find search not found");
    }

    /*Метод для нажатия на кнопку сохраненные списки статей*/
    @Step("Нажатие на кнопку сохраниет статью в список")
    public SaveListPage clickButtonSaveList() {
        waitForElementAndClick(buttonSaveList, "Button save list not found");
        return new SaveListPageFactory().get();
    }

}
