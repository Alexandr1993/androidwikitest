package tests;

import driver.Platform;
import factories.SearchPageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.MainPage;

public class SearchTests extends BaseTest {

    @Test(description = "Проверка текста строки поиска")
    public void checkTextSearch() {
        String textFindSearch = "Search Wikipedia";
        if (Platform.getInstance().isMw()) textFindSearch = "Search";
        Assert.assertEquals(
                new MainPage()
                        .buttonSkip()
                        .getTextArticle(),
                textFindSearch);

    }

    @Test(description = "Проверка поиска и отмены поиска")
    public void checkSearch() {
        Assert.assertTrue(
                new MainPage()
                        .buttonSkip()
                        .clickSearch()
                        .setTextFind("Java")
                        .getCollectionResultSearch()
                        .size() >= 4);
        new SearchPageFactory().get().clickCloseButton();
    }

    @Test(description = "Проверка отмены поиска и очистки")
    public void checkCancelSearch() {
        new MainPage()
                .buttonSkip()
                .clickSearch()
                .setTextFind("Java")
                .clickCloseButton()
                .setTextFind("Appium")
                .clearSearch();
    }

    @Test(description = "Проверка присутствия слова в результатх поиска")
    public void checkWordResult() {
        Assert.assertTrue(new MainPage()
                .buttonSkip()
                .clickSearch()
                .setTextFind("Java")
                .checkWordSearch("Java"));
    }

    @Test(description = "Проверка, что количество статей большу нуля")
    public void checkAmountOfNotEmptySearch() {
        Assert.assertTrue(new MainPage()
                .buttonSkip().clickSearch()
                .setTextFind("Linkin Park Discography")
                .getCollectionResultSearch().size() > 0, "No search results");

    }

    @Test(description = "Проверка отсутствия результатов поиска")
    public void checkForMissingSearchResults() {
        new MainPage()
                .buttonSkip()
                .clickSearch()
                .setTextFind("asdasdasfdffdfs")
                .checkTextNotResult()
                .assertElementNotPresent();
    }

}
