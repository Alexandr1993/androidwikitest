package web;

import page.ArticlePage;

public class WebArticlePage extends ArticlePage {

    static {
        title = "xpath://span[@class='mw-page-title-main']";
        futureText = "xpath://*[contains(@text, \"{SUBSTRING}\")]";
        buttonSave = "id:org.wikipedia:id/page_save";
        buttonAddToList = "xpath://*[contains(@text, \"Add to another reading list\")]";
        buttonCreateNew = "xpath://*[contains(@text, \t\"Create new\")]";
        fieldNameList = "id:org.wikipedia:id/text_input";
        buttonOk = "id:android:id/button1";
        buttonBack = "className:android.widget.ImageButton";
        nameList = "xpath://*[@text = '{SUBSTRING}']";
    }

}
